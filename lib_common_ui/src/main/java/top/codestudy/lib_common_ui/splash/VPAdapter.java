package top.codestudy.lib_common_ui.splash;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;

public class VPAdapter extends FragmentPagerAdapter {
    private static final String TAG = "VPAdapter";

    private List<Fragment> fragments;

    public VPAdapter(@NonNull FragmentManager fm, List<Fragment> fragments) {
        super(fm);
        if (fragments == null){
            throw new NullPointerException("fragments不能为null");
        }
        this.fragments = fragments;
    }

    public VPAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
