package top.codestudy.lib_common_ui.splash;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.circlenavigator.CircleNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.ColorTransitionPagerTitleView;

import java.util.ArrayList;
import java.util.List;

import top.codestudy.lib_common_ui.R;

public class SplashActivity extends AppCompatActivity {
    private static final String TAG = "SplashActivity";
    public final static int RESULT_CODE = 300;
    private ImageView mImgId;
    private ViewPager mVpAllInfo;
    private MagicIndicator mMagicIndicator;
    private TextView mBvDone;



    private String[] text_titles;
    private String[] text_contents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        //setStatusBarColor(R.color.lib_ui_black);
        mBvDone = findViewById(R.id.bv_done);
        mBvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CODE,null);
                finish();
            }
        });
        Intent intent = getIntent();
        if (intent == null){
            Toast.makeText(this,"显示更新变化信息异常！",Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        text_titles = intent.getStringArrayExtra("text_title");
        text_contents = intent.getStringArrayExtra("text_content");
        imgs = intent.getIntArrayExtra("imgs");
        if(text_titles!=null && text_contents!=null && imgs!=null
                && text_titles.length != text_contents.length && text_contents.length!=imgs.length&&text_titles.length!=imgs.length){
            Toast.makeText(this,"text_titles/text_contents/imgs数组长度须一致！",Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        initView();

    }

    public void setStatusBarColor(int colorId) {
        Window window = getWindow();
        window.setStatusBarColor(getResources().getColor(colorId));
    }

    private void initView() {
        mImgId = findViewById(R.id.img_id);
        mImgId.setImageResource(imgs[0]);
        mVpAllInfo = findViewById(R.id.vp_all_info);
        initViewPage();
        mMagicIndicator = findViewById(R.id.magic_indicator);
        initMagicIndicator();

    }
    private int[] imgs = new int[]{R.drawable.girl,R.drawable.girl,R.drawable.girl};
    private void initViewPage() {
        List<Fragment> fragmentList = new ArrayList<>();
        for(int i=0;i<text_titles.length;i++){
            fragmentList.add(ShowChangeFragment.newInstance( text_titles[i] , text_contents[i] ));
        }
        mVpAllInfo.setAdapter(new VPAdapter(getSupportFragmentManager(),fragmentList));

        mVpAllInfo.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            int current = 0;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                //Log.e(TAG, "onPageScrolled: positionOffset => " + positionOffset + " ;  position => " + position + "; positionOffsetPixels => " + positionOffsetPixels);
                if (position == current){
                    mImgId.setImageAlpha((int)(255 - 255 * positionOffset));
                }else {
                    mImgId.setImageAlpha((int)(255 * positionOffset));
                }
            }

            @Override
            public void onPageSelected(int position) {
                Log.d(TAG, "onPageSelected:  position " + position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                Log.d(TAG, "onPageScrollStateChanged: state => " + state);
                if (state == 0 ){
                    current = mVpAllInfo.getCurrentItem();
                    mImgId.setImageResource(imgs[current]);
                    mImgId.setImageAlpha(255);
                    Log.e(TAG, "onPageScrollStateChanged: current ==> " + current);
                    if (current == text_titles.length-1){
                        mBvDone.setVisibility(View.VISIBLE);
                    }else {
                        mBvDone.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    private void initMagicIndicator() {
        CircleNavigator circleNavigator = new CircleNavigator(this);
        circleNavigator.setCircleCount(text_titles.length);
        circleNavigator.setCircleColor(Color.parseColor("#CFDF00"));
        circleNavigator.setCircleClickListener(new CircleNavigator.OnCircleClickListener() {
            @Override
            public void onClick(int index) {
                mVpAllInfo.setCurrentItem(index);
            }
        });
        mMagicIndicator.setNavigator(circleNavigator);
        ViewPagerHelper.bind(mMagicIndicator, mVpAllInfo);
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CODE,null);
        finish();
    }
}