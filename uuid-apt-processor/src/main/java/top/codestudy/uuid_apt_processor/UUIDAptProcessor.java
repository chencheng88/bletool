package top.codestudy.uuid_apt_processor;


import com.google.auto.service.AutoService;
import com.squareup.javapoet.JavaFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import javax.tools.Diagnostic;

import top.codestudy.annotation_uuid.MyUUID;

@AutoService(Processor.class)
public class UUIDAptProcessor extends AbstractProcessor {

    private Messager mMessager;
    private Elements mElementUtils;
    //private Map<String, ClassCreatorProxy> mProxyMap = new HashMap<>();
    private boolean writeDone = false;
    @Override
    public Set<String> getSupportedOptions() {
        return super.getSupportedOptions();
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        Set<String> supportedAnnotationTypes = new HashSet<>();
        supportedAnnotationTypes.add(MyUUID.class.getCanonicalName());
        return supportedAnnotationTypes;
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        //操作Element的工具类
        mElementUtils = processingEnv.getElementUtils();
        //打印消息的类
        mMessager = processingEnv.getMessager();
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnvironment) {
        mMessager.printMessage(Diagnostic.Kind.WARNING, "processing........................");
        if (writeDone){
            return false;
        }
        Map<String,String> uuidMap = new HashMap<>();
        //得到所有的注解
        Set<? extends Element> elements = roundEnvironment.getElementsAnnotatedWith(MyUUID.class);
        for (Element element : elements) {
            TypeElement classElement = (TypeElement) element;
            //得到类的全限定名
            String fullClassName = classElement.getQualifiedName().toString();
            //得到uuid
            MyUUID uuid = classElement.getAnnotation(MyUUID.class);
            mMessager.printMessage(Diagnostic.Kind.WARNING, "fullClassName = " + fullClassName + " ; uuid = " + uuid.uuid());
            uuidMap.put(uuid.uuid(), fullClassName);
        }
        //通过javapoet生成
        ClassBuilder proxyInfo = new ClassBuilder(uuidMap);
        JavaFile javaFile = JavaFile.builder(proxyInfo.getPackageName(), proxyInfo.generateJavaCode()).build();
        try {
            //　生成文件
            javaFile.writeTo(processingEnv.getFiler());
            writeDone = true;
        } catch (IOException e) {
            e.printStackTrace();
            mMessager.printMessage(Diagnostic.Kind.WARNING, "process IOException ...");
        }
        mMessager.printMessage(Diagnostic.Kind.WARNING, "process finish ...");
        return true;
    }
}