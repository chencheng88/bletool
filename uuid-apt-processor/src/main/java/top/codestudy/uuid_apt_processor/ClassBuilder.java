package top.codestudy.uuid_apt_processor;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;

public class ClassBuilder {

    private static Map<String,String> mUuidMap = new HashMap<>();
    private final String mClassName = "DataTranslate";
    private TypeElement mTypeElement;
    public ClassBuilder(Map<String,String> uuidMap){
        mUuidMap = uuidMap;
    }

    public TypeSpec generateJavaCode(){
        TypeSpec bindingClass = TypeSpec.classBuilder(mClassName)
                .addModifiers(Modifier.PUBLIC)
                .addMethod(generateMethods())
                .addField(generateField())
                .build();
        return bindingClass;
    }

    private CodeBlock generateCodeBlock() {

      return null;
    }

    private FieldSpec generateField() {

        FieldSpec.Builder builder = FieldSpec.builder(Map.class,"uuidMap",Modifier.PUBLIC,Modifier.STATIC);

        return builder.build();
    }

    private MethodSpec generateMethods() {

        MethodSpec.Builder methodBuilder = MethodSpec.methodBuilder("init")
                .addModifiers(Modifier.PUBLIC)
                .addException(Exception.class)
                .returns(void.class);
        methodBuilder.addCode("uuidMap = new java.util.HashMap<String,Class<?>>();\n");

        for (Map.Entry<String,String> entry : mUuidMap.entrySet()) {
            String uuidName = entry.getKey();
            String className = entry.getValue();
            methodBuilder.addCode("uuidMap.put(\"" + uuidName + "\" , Class.forName(\"" + className +"\" ));\n");
        }

        return methodBuilder.build();
    }

    public String getPackageName(){
        return "top.codestudy.uuid";
    }
}
