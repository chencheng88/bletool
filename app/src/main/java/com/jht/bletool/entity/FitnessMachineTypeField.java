package com.jht.bletool.entity;

public class FitnessMachineTypeField {
    private static final String TAG = "FitnessMachineTypeField";
    private String msg = "";

    private boolean isFitnessMachineAvailable = false;
    private boolean TreadmillSupported = false;
    private boolean CrossTrainerSupported = false;
    private boolean StepClimberSupported = false;
    private boolean StairClimberSupported = false;
    private boolean RowerSupported = false;
    private boolean IndoorBikeSupported = false;

    public FitnessMachineTypeField(byte[] bytes) {
        parseData(bytes);
    }

    public void parseData(byte[] buffer) {

        //Fitness Machine Features Field
        isFitnessMachineAvailable = ((buffer[0] & 0x01) == 0x01);
        if (isFitnessMachineAvailable) {
            msg += "Fitness Machine Available;\n";
        }else {
            msg += "Fitness Machine UnAvailable;\n";
        }

        TreadmillSupported = ((buffer[2] & 0x01) == 0x01);
        CrossTrainerSupported = ((buffer[2] & 0x02) == 0x02);
        StepClimberSupported = ((buffer[2] & 0x04) == 0x04);
        StairClimberSupported = ((buffer[2] & 0x04) == 0x04);
        RowerSupported = ((buffer[2] & 0x10) == 0x10);
        IndoorBikeSupported = ((buffer[2] & 0x20) == 0x20);

        if(TreadmillSupported){
            msg +=  "Treadmill Supported;\n";
        }
        if(CrossTrainerSupported){
            msg +=  "CrossTrainer Supported;\n";
        }
        if(StepClimberSupported){
            msg +=  "StepClimber Supported;\n";
        }
        if(StairClimberSupported){
            msg +=  "StairClimber Supported;\n";
        }
        if(RowerSupported){
            msg +=  "Rower Supported;\n";
        }
        if(IndoorBikeSupported){
            msg +=  "IndoorBike Supported;\n";
        }

    }

    public String convert2String() {
        return msg;
    }
}
