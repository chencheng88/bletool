package com.jht.bletool.entity;

import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.TextView;

public class DataLog implements Parcelable {

    String date;
    String ble_data_type;
    String ble_data;
    int text_color;

    public DataLog() {
    }

    public DataLog(String date, String ble_data_type, String ble_data, int text_color) {
        this.date = date;
        this.ble_data_type = ble_data_type;
        this.ble_data = ble_data;
        this.text_color = text_color;
    }


    protected DataLog(Parcel in) {
        date = in.readString();
        ble_data_type = in.readString();
        ble_data = in.readString();
        text_color = in.readInt();
    }

    public static final Creator<DataLog> CREATOR = new Creator<DataLog>() {
        @Override
        public DataLog createFromParcel(Parcel in) {
            return new DataLog(in);
        }

        @Override
        public DataLog[] newArray(int size) {
            return new DataLog[size];
        }
    };

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getBle_data_type() {
        return ble_data_type;
    }

    public void setBle_data_type(String ble_data_type) {
        this.ble_data_type = ble_data_type;
    }

    public String getBle_data() {
        return ble_data;
    }

    public void setBle_data(String ble_data) {
        this.ble_data = ble_data;
    }

    public int getText_color() {
        return text_color;
    }

    public void setText_color(int text_color) {
        this.text_color = text_color;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(date);
        dest.writeString(ble_data_type);
        dest.writeString(ble_data);
        dest.writeInt(text_color);
    }

}
