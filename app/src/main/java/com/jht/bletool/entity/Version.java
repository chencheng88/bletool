package com.jht.bletool.entity;

import java.util.ArrayList;
import java.util.List;

public class Version {
    private String versionName;
    private String versionCode;
    private List<String> item = new ArrayList<>();

    public Version(){
    }

    public Version(String versionName, String versionCode, List<String> item) {
        this.versionName = versionName;
        this.versionCode = versionCode;
        this.item = item;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public String getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }

    public List<String> getItem() {
        return item;
    }

    public void setItem(List<String> item) {
        this.item = item;
    }

    @Override
    public String toString() {
        return "Version{<br\\>" +
                "&nbsp&nbsp&nbsp&nbsp versionName='" + versionName + '\'' +
                ",<br\\> &nbsp&nbsp&nbsp&nbsp versionCode='" + versionCode + '\'' +
                ",<br\\> &nbsp&nbsp&nbsp&nbsp item=<br\\>" + item +
                " <br\\>} <br\\>";
    }
}
