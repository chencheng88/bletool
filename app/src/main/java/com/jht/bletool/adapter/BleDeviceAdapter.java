package com.jht.bletool.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hjq.toast.ToastUtils;
import com.hjq.xtoast.OnClickListener;
import com.hjq.xtoast.XToast;
import com.jht.bletool.APP;
import com.jht.bletool.R;
import com.jht.bletool.entity.BleDevice;
import com.jht.bletool.ui.DataTransmissionActivity;
import com.jht.bletool.util.BaseUtils;
import com.jht.bletool.widget.ChangeColorTextView;

import java.util.ArrayList;
import java.util.List;

public class BleDeviceAdapter extends RecyclerView.Adapter<BleDeviceAdapter.BleDeviceViewHolder> {
    private static final String TAG = "BleDeviceAdapter";
    public int count = 1;
    private Context mContext;
    private List<BleDevice> ble_data = new ArrayList<>();

    public BleDeviceAdapter(Context mContext) {
        this.mContext = mContext;
    }
    private XToast showBleBroadcastData;
    @NonNull
    @Override
    public BleDeviceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.i(TAG, "onCreateViewHolder: ");
        View inflate = LayoutInflater.from(mContext).inflate(viewType, parent, false);
        return new BleDeviceViewHolder(inflate,viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull BleDeviceViewHolder viewHolder, final int position) {
        int itemViewType = getItemViewType(position);
        if(itemViewType == R.layout.layout_no_data){
            return;
        }
        if (itemViewType == R.layout.item_ble_device){
            final BleDevice bleDevice = ble_data.get(position);
            viewHolder.mBtnDeviceName.setText(bleDevice.getDevice().getName());
            viewHolder.mBtnDeviceMac.setText(bleDevice.getDevice().getAddress());
            viewHolder.mTvBleDbm.setText(""+bleDevice.getRssi()+" dBm");
            viewHolder.mBtnConnectBle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setClass(APP.getAppContext(), DataTransmissionActivity.class);
                    intent.putExtra("ble_device",bleDevice.getDevice());
                    intent.putExtra("scan_record",bleDevice.getMachineType());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    APP.getAppContext().startActivity(intent);
                }
            });

            viewHolder.mLlBleLayoutParent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showBleBroadcastDataRecord(position);
                }
            });
        }
    }
    private void showBleBroadcastDataRecord(int position){
        if(mContext == null){
            return;
        }
        if (showBleBroadcastData == null){
            showBleBroadcastData = new XToast((Activity)mContext)
                    .setView(R.layout.dialog_show_ble_broadcast_data)
                    .setDuration(0)
                    // 设置动画样式
                    .setAnimStyle(R.style.go_setting_anim_style)
                    .setOnClickListener(R.id.btn_close_ble_broadcast_dialog, new OnClickListener<Button>() {
                        @Override
                        public void onClick(XToast toast, Button view) {
                            toast.cancel();
                        }
                    });
        }
        ((TextView)(showBleBroadcastData.findViewById(R.id.tv_ble_broadcast_data))).setText("ble广播数据：\n"+BaseUtils.getHexStringNoSplit(ble_data.get(position).getScanRecord()));
        showBleBroadcastData.show();
    }

    @Override
    public void onBindViewHolder(@NonNull BleDeviceViewHolder holder, int position, @NonNull List<Object> payloads) {
        if (payloads == null || payloads.size() ==0){
            super.onBindViewHolder(holder, position, payloads);
        }else {
            BleDevice bleDevice = ble_data.get(position);
            for (Object payload:payloads) {
                switch (String.valueOf(payload)){
                    case "mTvBleDbm":
                        holder.mTvBleDbm.setText(bleDevice.getRssi()+ " dBm");
                        holder.mTvBleDbm.setTextColorIn2S();
                        break;
                    default:
                        break;
                }
            }
        }


    }

    @Override
    public int getItemCount() {
        return ble_data.size() == 0 ? 1 : ble_data.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (ble_data.size() == 0 ) {
            return R.layout.layout_no_data;
        }
        return R.layout.item_ble_device;
    }

    public void addDevice(BleDevice device) {
        updateDeviceData(device);
    }

    public void updateDeviceData(BleDevice device) {
        //首先检测是否存在次device
        for (int i = 0; i < ble_data.size(); i++) {
            BleDevice bleDevice = ble_data.get(i);
            if (bleDevice.getDevice().getAddress().equals(device.getDevice().getAddress())) {
                if (bleDevice.getRssi() != device.getRssi()){
                    bleDevice.setRssi(device.getRssi());
                    bleDevice.setChangeRSSI(true);
                    notifyItemChanged(i,"mTvBleDbm");
                }
                bleDevice.setScanRecord(device.getScanRecord());
                return;
            }
        }
        ble_data.add(device);
        notifyItemInserted(ble_data.size()+1);
    }


    public void clear() {
        ble_data.clear();
        count = 1;
        notifyDataSetChanged();
    }

    public class BleDeviceViewHolder extends RecyclerView.ViewHolder {

        TextView mBtnDeviceName;
        TextView mBtnDeviceMac;
        ChangeColorTextView mTvBleDbm;
        Button mBtnConnectBle;
        LinearLayout mLlBleLayoutParent;

        public BleDeviceViewHolder(@NonNull View itemView,int viewType) {
            super(itemView);
            if (viewType == R.layout.item_ble_device){
                mBtnDeviceName = itemView.findViewById(R.id.btn_device_name);
                mBtnDeviceMac = itemView.findViewById(R.id.btn_device_mac);
                mTvBleDbm = itemView.findViewById(R.id.tv_ble_dbm);
                mBtnConnectBle = itemView.findViewById(R.id.btn_connect_ble);
                mLlBleLayoutParent = itemView.findViewById(R.id.ll_ble_layout_parent);
            }

        }
    }
}
