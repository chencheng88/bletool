package com.jht.bletool.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jht.bletool.APP;
import com.jht.bletool.R;
import com.jht.bletool.entity.BleDevice;
import com.jht.bletool.entity.DataLog;
import com.jht.bletool.ui.DataTransmissionActivity;
import com.jht.bletool.widget.ChangeColorTextView;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class BlePrintLogAdapter extends RecyclerView.Adapter<BlePrintLogAdapter.BleLogViewHolder> {
    private static final String TAG = "BleDeviceAdapter";
    private Context mContext;

    private List<DataLog> data_log = new ArrayList<>();

    public BlePrintLogAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public BlePrintLogAdapter.BleLogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_print_log, parent, false);

        return new BlePrintLogAdapter.BleLogViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BlePrintLogAdapter.BleLogViewHolder holder, int position) {
        DataLog dataLog = data_log.get(position);
        holder.mTvSendTime.setText(dataLog.getDate());
        holder.mTvBleDataType.setText(dataLog.getBle_data_type());
        holder.mTvBleDataType.setTextColor(dataLog.getText_color());
        holder.mTvBleData.setText(dataLog.getBle_data());
        holder.mTvBleData.setTextColor(dataLog.getText_color());
    }

    @Override
    public int getItemCount() {
        return data_log.size();
    }

    public void addData(DataLog dataLog){
        data_log.add(dataLog);
        notifyItemChanged(data_log.size());

    }

    public void clear(){
        data_log.clear();
        notifyDataSetChanged();
    }

    public class BleLogViewHolder extends RecyclerView.ViewHolder {
        TextView mTvSendTime;
        TextView mTvBleDataType;
        TextView mTvBleData;


        public BleLogViewHolder(@NonNull View view) {
            super(view);
            mTvSendTime = view.findViewById(R.id.tv_send_time);
            mTvBleDataType = view.findViewById(R.id.tv_ble_data_type);
            mTvBleData = view.findViewById(R.id.tv_ble_data);

        }
    }
}
