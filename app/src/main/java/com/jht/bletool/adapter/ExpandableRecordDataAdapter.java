package com.jht.bletool.adapter;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jht.bletool.APP;
import com.jht.bletool.R;
import com.jht.bletool.config.AppConfig;
import com.jht.bletool.util.AttributeLookup;
import com.jht.bletool.widget.Rotate180ImageView;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public class ExpandableRecordDataAdapter extends BaseExpandableListAdapter {

    public interface Callback{
        //将要执行的组数据
        void groupDataExec(String groupUUID,Map<String,List<String>> allData);
        //将要删除的组
        void groupDataDel(String groupUUID,Map<String,List<String>> allData);
        //具体执行某组下面的数据
        void childDataExec(String groupUUID,String childCommData);
        //具体删除某组下面的数据
        void childDataDel(String groupUUID,String childCommData);

    }

    private Callback callback;
    private static final String TAG = "ExpandableRecordDataAda";
    private Context context;
    private List<String> character;
    private Map<String,List<String>> character_map_recordData;

    public ExpandableRecordDataAdapter(Context context, List<String> character, Map<String,List<String>> character_map_recordData) {
        this.context = context;
        this.character = character;
        this.character_map_recordData = character_map_recordData;
    }

    public Callback getCallback() {
        return callback;
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    public int getGroupCount() {
        return character.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return character_map_recordData.get(character.get(groupPosition)).size();
    }

    @Override
    public String getGroup(int groupPosition) {
        return character.get(groupPosition);
    }

    @Override
    public String getChild(int groupPosition, int childPosition) {
        return character_map_recordData.get(character.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        ExpandableRecordDataAdapter.GroupViewHolder groupViewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.dialog_record_adapter_group, parent, false);
            groupViewHolder = new ExpandableRecordDataAdapter.GroupViewHolder();
            groupViewHolder.mTvGroupRecordDataChar = convertView.findViewById(R.id.tv_group_record_data_char);
            groupViewHolder.mIvGroupPlayRecordData = convertView.findViewById(R.id.iv_group_play_record_data);
            groupViewHolder.mIvGroupDelRecordData = convertView.findViewById(R.id.iv_group_del_record_data);
            convertView.setTag(groupViewHolder);
        } else {
            groupViewHolder = (ExpandableRecordDataAdapter.GroupViewHolder) convertView.getTag();
        }
        groupViewHolder.mTvGroupRecordDataChar.setText(character.get(groupPosition));
        groupViewHolder.mIvGroupPlayRecordData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: group mIvGroupPlayRecordData!");
                if(callback!=null){
                    //callback.groupDataExec(character.get(groupPosition),character_map_recordData);
                }

            }
        });
        groupViewHolder.mIvGroupDelRecordData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: group mIvGroupDelRecordData!");
                List<String> strings = character_map_recordData.get(character.get(groupPosition));
                strings.clear();
                String remove = null;

                //移除这个组
                remove = character.remove(groupPosition);

                notifyDataSetChanged();
                SharedPreferences record_data = APP.getAppContext().getSharedPreferences(AppConfig.record_data, Context.MODE_PRIVATE);
                SharedPreferences.Editor edit = record_data.edit();
                edit.remove(remove);
                edit.apply();

                if(callback!=null){
                    //callback.groupDataDel(character.get(groupPosition),character_map_recordData);
                }
            }
        });
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ExpandableRecordDataAdapter.ChildViewHolder childViewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.dialog_record_adapter_child, parent, false);
            childViewHolder = new ExpandableRecordDataAdapter.ChildViewHolder();

            childViewHolder.tv_child_record_data_comm = convertView.findViewById(R.id.tv_child_record_data_comm);
            childViewHolder.tv_child_record_data_data = convertView.findViewById(R.id.tv_child_record_data_data);
            childViewHolder.mIvChildPlayRecordData = convertView.findViewById(R.id.iv_child_play_record_data);
            childViewHolder.mIvChildDelRecordData = convertView.findViewById(R.id.iv_child_del_record_data);

            convertView.setTag(childViewHolder);
        } else {
            childViewHolder = (ExpandableRecordDataAdapter.ChildViewHolder) convertView.getTag();
        }
        String[] split = character_map_recordData.get(character.get(groupPosition)).get(childPosition).split("@");
        if (split.length == 1){
            childViewHolder.tv_child_record_data_comm.setText(split[0]);
            childViewHolder.tv_child_record_data_data.setText("null");

        }else if(split.length == 2){
            childViewHolder.tv_child_record_data_comm.setText(split[0]);
            childViewHolder.tv_child_record_data_data.setText(split[1]);
        }

        childViewHolder.mIvChildPlayRecordData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: child mIvChildPlayRecordData!");
                if(callback!=null){
                    callback.childDataExec(character.get(groupPosition),character_map_recordData.get(character.get(groupPosition)).get(childPosition));
                }
            }
        });
        childViewHolder.mIvChildDelRecordData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: child mIvChildDelRecordData!");
                List<String> strings = character_map_recordData.get(character.get(groupPosition));
                strings.remove(childPosition);
                String remove = null;
                if(strings.size() == 0){
                    //移除这个组
                    remove = character.remove(groupPosition);
                }
                notifyDataSetChanged();
                SharedPreferences record_data = APP.getAppContext().getSharedPreferences(AppConfig.record_data, Context.MODE_PRIVATE);
                SharedPreferences.Editor edit = record_data.edit();
                StringBuilder sb = new StringBuilder();
                for(int i=0;i<strings.size();i++){
                    sb.append(strings.get(i));
                    if(i!=strings.size()-1){
                        sb.append(";");
                    }
                }
                if("".equals(sb.toString())){
                    edit.remove(remove);
                }else{
                    edit.putString(character.get(groupPosition),sb.toString());
                }
                edit.apply();
                if(callback != null){
                    //callback.childDataDel(character.get(groupPosition),character_map_recordData.get(character.get(groupPosition)).get(childPosition));
                }
            }
        });

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    static class GroupViewHolder {
        TextView mTvGroupRecordDataChar;
        ImageView mIvGroupPlayRecordData;
        ImageView mIvGroupDelRecordData;

    }


    static class ChildViewHolder {
        TextView tv_child_record_data_comm;
        TextView tv_child_record_data_data;
        ImageView mIvChildPlayRecordData;
        ImageView mIvChildDelRecordData;

    }
}
