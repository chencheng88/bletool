package com.jht.bletool.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.jht.bletool.util.NullUtil;

import java.util.List;

public class MyViewPageAdapter2 extends FragmentPagerAdapter {

    private static final String TAG = "MyViewPageAdapter2";
    private String[] header_str = {"蓝牙服务", "实时日志"};
    List<Fragment> data;

    public MyViewPageAdapter2(@NonNull FragmentManager fm,List<Fragment> fragments) {
        super(fm);
        NullUtil.checkObjectNull(fragments);
        this.data = fragments;
    }

    public MyViewPageAdapter2(@NonNull FragmentManager fm, int behavior,List<Fragment> fragments) {
        super(fm, behavior);
        NullUtil.checkObjectNull(fragments);
        this.data = fragments;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return data.get(position);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return header_str[position];
    }

}

