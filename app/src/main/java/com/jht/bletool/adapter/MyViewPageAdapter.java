package com.jht.bletool.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.jht.bletool.util.NullUtil;

import java.util.List;

public class MyViewPageAdapter extends FragmentPagerAdapter {

    private static final String TAG = "MyViewPageAdapter";
    private String[] header_str = {"可用设备", "活动设备"};
    List<Fragment> data;

    public MyViewPageAdapter(@NonNull FragmentManager fm,List<Fragment> fragments) {
        super(fm);
        NullUtil.checkObjectNull(fragments);
        this.data = fragments;
    }

    public MyViewPageAdapter(@NonNull FragmentManager fm, int behavior,List<Fragment> fragments) {
        super(fm, behavior);
        NullUtil.checkObjectNull(fragments);
        this.data = fragments;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return data.get(position);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return header_str[position];
    }

}
