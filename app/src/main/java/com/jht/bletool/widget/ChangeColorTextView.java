package com.jht.bletool.widget;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.annotation.Nullable;

public class ChangeColorTextView extends androidx.appcompat.widget.AppCompatTextView {

    public ChangeColorTextView(Context context) {
        super(context);
    }

    public ChangeColorTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ChangeColorTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private Runnable run = new Runnable() {
        @Override
        public void run() {
            ChangeColorTextView.super.setTextColor(Color.parseColor("#8A8484"));
        }
    };

    public void setTextColorIn2S() {
        super.setTextColor(Color.BLACK);
        //removeCallbacks(run);

        postDelayed(run,1000);
    }
}
