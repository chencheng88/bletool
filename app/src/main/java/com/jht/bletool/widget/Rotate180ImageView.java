package com.jht.bletool.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.ImageView;

public class Rotate180ImageView extends androidx.appcompat.widget.AppCompatImageView {
    public Rotate180ImageView(Context context) {
        super(context);
    }

    public Rotate180ImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Rotate180ImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        canvas.rotate(180,getMeasuredWidth()/2.0f,getMeasuredHeight()/2.0f);

        super.onDraw(canvas);
    }
}
