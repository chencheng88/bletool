package com.jht.bletool.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

class ClickedImagView extends androidx.appcompat.widget.AppCompatImageView {

    private boolean isClick = false;
    public ClickedImagView(Context context) {
        super(context);
    }

    public ClickedImagView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ClickedImagView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


}
