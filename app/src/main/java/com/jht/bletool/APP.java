package com.jht.bletool;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.util.Log;

import androidx.preference.PreferenceManager;

import com.hjq.toast.ToastUtils;
import com.jht.bletool.config.AppConfig;
import com.jht.bletool.util.SelectParseClass;

import top.codestudy.uuid.DataTranslate;

public class APP extends Application {
    private static Context context = null;
    private static final String TAG = "APP";
    @Override
    public void onCreate() {
        super.onCreate();
        ToastUtils.init(this);
        context = getApplicationContext();
        //通过new的形式初始化static 代码块；不能删除
        new SelectParseClass();
        try {
            new DataTranslate().init();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "onCreate: DataTranslate error!");
        }
        ApplicationInfo applicationInfo = getApplicationInfo();
        Log.i(TAG, "onCreate: BuildConfig.VERSION_NAME = " + BuildConfig.VERSION_NAME +" ; VERSION_CODE =" + BuildConfig.VERSION_CODE);
        initAppConfig();

    }

    private void initAppConfig() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (!sharedPreferences.contains("scan_time")) {
            Log.i(TAG, "initAppConfig: sharedPreferences.contains(\"scan_time\") ");
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString("scan_time","20000");
            edit.commit();
            AppConfig.init(Long.parseLong("20000",10));
        }else {
            String scan_time = sharedPreferences.getString("scan_time", "20000");
            AppConfig.init(Long.parseLong(scan_time,10));
        }
    }

    public static Context getAppContext(){
        return context;
    }

}
