package com.jht.bletool.characteristic.deviceinfromations;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import androidx.annotation.Keep;

import top.codestudy.annotation_uuid.MyUUID;
import com.jht.bletool.characteristic.TranslateData;

import java.io.UnsupportedEncodingException;

@Keep
@MyUUID(uuid = "00002a29-0000-1000-8000-00805f9b34fb")
public class ManufacturerNameString implements TranslateData {
    private static final String TAG = "ManufacturerNameString";
    private String manufacturerNameString = "";
    private byte[] value;

    public ManufacturerNameString() {
    }

    public ManufacturerNameString(BluetoothGattCharacteristic characteristic) {
        value = characteristic.getValue();
        try {
            manufacturerNameString += new String(value, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            Log.e(TAG, "ManufacturerNameString: error");
        }
    }

    @Override
    public String convert2String() {
        return "Manufacturer Name: " + manufacturerNameString+"\n";
    }

    @Override
    public String merge(TranslateData translateData) {
        return null;
    }
    @Override
    public boolean hasMoreData() {
        return false;
    }

    @Override
    public byte[] getData() {
        return value;
    }

    @Override
    public boolean isInvalidData() {
        return false;
    }
}
