package com.jht.bletool.characteristic.ftms;


import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import androidx.annotation.Keep;

import top.codestudy.annotation_uuid.MyUUID;
import com.jht.bletool.characteristic.TranslateData;
import com.jht.bletool.util.BaseUtils;

@Keep
@MyUUID(uuid = "00002ad7-0000-1000-8000-00805f9b34fb")
public class SupportedHeartRateRange implements TranslateData {
    private final String TAG = "SupportedHeartRateRange";
    private static SupportedHeartRateRange data;

    private byte[] MinimumHeartRate = new byte[1];
    private byte[] MaximumHeartRate = new byte[1];
    private byte[] MinimumIncrement = new byte[1];
    private byte[] valueData;
    public static SupportedHeartRateRange getInstance() {
        data = new SupportedHeartRateRange();
        return data;
    }

    public SupportedHeartRateRange() {
    }
    public SupportedHeartRateRange(BluetoothGattCharacteristic characteristic){
        byte[] value = characteristic.getValue();
        valueData = value;
        parseData(value);
        for(int i=0;i<value.length;i++){
            Log.i(TAG, "SupportedHeartRateRange: i = " + i +"; ox" + Integer.toHexString(BaseUtils.byte1ToInt(value[i])));
        }
    }
    public void parseData(byte[] buffer) {
        System.arraycopy(buffer, 0, MinimumHeartRate, 0, 1);
        System.arraycopy(buffer, 1, MaximumHeartRate, 0, 1);
        System.arraycopy(buffer, 2, MinimumIncrement, 0, 1);
    }

    public int getMinimumHeartRate() {
        return BaseUtils.byte1ToInt(MinimumHeartRate[0]);
    }

    public int getMaximumHeartRate() {
        return BaseUtils.byte1ToInt(MaximumHeartRate[0]);
    }

    public int getMinimumIncrement() {
        return BaseUtils.byte1ToInt(MinimumIncrement[0]);
    }

    @Override
    public String convert2String() {
        return "SupportedHeartRateRange:\nMinimumHeartRate : " + getMinimumHeartRate()
                + " bpm \n MaximumHeartRate : " + getMaximumHeartRate()
                + " bpm \n MinimumIncrement : " + getMinimumIncrement() + " bpm "+"\n";
    }

    @Override
    public String merge(TranslateData translateData) {
        return null;
    }
    @Override
    public boolean hasMoreData() {
        return false;
    }

    @Override
    public byte[] getData() {
        return valueData;
    }

    @Override
    public boolean isInvalidData() {
        return false;
    }
}
