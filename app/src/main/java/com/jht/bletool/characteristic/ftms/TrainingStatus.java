package com.jht.bletool.characteristic.ftms;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import androidx.annotation.Keep;

import top.codestudy.annotation_uuid.MyUUID;
import com.jht.bletool.characteristic.TranslateData;
import com.jht.bletool.util.BaseUtils;

import java.io.UnsupportedEncodingException;

@Keep
@MyUUID(uuid = "00002ad3-0000-1000-8000-00805f9b34fb")
public class TrainingStatus implements TranslateData {
    private final String TAG = "TrainingStatus";
    private static TrainingStatus data;
    private String trainingStatusString = "";
    private byte[] trainingStatusByte ;
    private boolean isTrainingStatusStringCurrent = false;
    byte[] value1;
    private byte[] status = new byte[1];
    private byte[] valueData;
    public static TrainingStatus getInstance()
    {
        data = new TrainingStatus();
        return data;
    }
    private TrainingStatus(){}
    public TrainingStatus(BluetoothGattCharacteristic characteristic){
        byte[] value = characteristic.getValue();
        value1 = value;
        valueData = value;
        parseData(value);
        for(int i=0;i<value.length;i++){
            Log.i(TAG, "TrainingStatus: i = " + i +"; ox" + Integer.toHexString(BaseUtils.byte1ToInt(value[i])));
        }
    }
    public void parseData(byte[] buffer)  {
        Log.i(TAG, "parseData: buffer.length " + buffer.length);
        System.arraycopy(buffer, 1, status, 0, 1);

        if ((buffer[0] & 0x01) == 0x01){
            if (buffer.length > 2){
                Log.d(TAG, "parseData: TrainingStatusStringCurrent");
                isTrainingStatusStringCurrent = true;
                trainingStatusByte = new byte[buffer.length-2];
                System.arraycopy(buffer, 2, trainingStatusByte, 0, buffer.length-2);
                try {
                    trainingStatusString += new String(trainingStatusByte, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    trainingStatusString = "error: UTF-8 encoding exception!";
                }
            }
        }

    }

    public String getTrainingStatus(){
        switch (status[0]){
            case 0x00:
                return "Other";
            case 0x01:
                return "Idle";
            case 0x02:
                return "Warming Up";
            case 0x03:
                return "Low Intensity Interval";
            case 0x04:
                return "High Intensity Interval";
            case 0x05:
                return "Recovery Interval";
            case 0x06:
                return "Isometric";
            case 0x07:
                return "Heart Rate Control";
            case 0x08:
                return "Fitness Test";
            case 0x09:
                return "Speed Outside of Control Region - Low (increase speed to return to controllable \n" +
                        "region)";
            case 0x0A:
                return "Speed Outside of Control Region - High (decrease speed to return to controllable \n" +
                        "region)";
            case 0x0B:
                return "Cool Down";
            case 0x0C:
                return "Watt Control";
            case 0x0D:
                return "Manual Mode (Quick Start)";
            case 0x0E:
                return "Pre-Workout";
            case 0x0F:
                return "Post-Workout";
            default:
                return "Reserved for Future Use";
        }
    }

    @Override
    public String convert2String() {
        String temp = "TrainingStatus is "+getTrainingStatus()+" ;\n ";
        if (isTrainingStatusStringCurrent){
            temp += trainingStatusString +" ;\n";
        }
        return  temp ;
    }

    @Override
    public String merge(TranslateData translateData) {
        return null;
    }

    @Override
    public boolean hasMoreData() {
        return false;
    }

    @Override
    public byte[] getData() {
        return valueData;
    }

    @Override
    public boolean isInvalidData() {
        return false;
    }
}
