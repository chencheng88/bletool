package com.jht.bletool.characteristic.userdata;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import androidx.annotation.Keep;

import top.codestudy.annotation_uuid.MyUUID;
import com.jht.bletool.characteristic.TranslateData;

import java.io.UnsupportedEncodingException;

@Keep
@MyUUID(uuid = "00002ab5-0000-1000-8000-00805f9b34fb")
public class LocationName implements TranslateData {
    private static final String TAG = "LocationName";
    private String locationName = "";
    private byte[] value;

    public LocationName(){

    }

    public LocationName(BluetoothGattCharacteristic characteristic) {
        value = characteristic.getValue();
        try {
            locationName += new String(value, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            Log.e(TAG, "LocationName: error");
        }
    }

    @Override
    public String convert2String() {
        return "LocationName: " + locationName+"\n";
    }

    @Override
    public String merge(TranslateData translateData) {
        return null;
    }

    @Override
    public boolean hasMoreData() {
        return false;
    }

    @Override
    public byte[] getData() {
        return value;
    }

    @Override
    public boolean isInvalidData() {
        return false;
    }
}
