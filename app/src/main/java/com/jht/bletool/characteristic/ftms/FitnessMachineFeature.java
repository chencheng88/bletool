package com.jht.bletool.characteristic.ftms;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import androidx.annotation.Keep;

import top.codestudy.annotation_uuid.MyUUID;
import com.jht.bletool.characteristic.TranslateData;
import com.jht.bletool.util.BaseUtils;

@Keep
@MyUUID(uuid = "00002acc-0000-1000-8000-00805f9b34fb")
public class FitnessMachineFeature implements TranslateData {
    private final String TAG = "FitnessMachineFeature";
    private static FitnessMachineFeature data;
    //Fitness Machine Features Field
    private boolean isAverageSpeedSupported;
    private boolean isCadenceSupported;
    private boolean isTotalDistanceSupported;
    private boolean isInclinationSupported;
    private boolean isElevationGainSupported;
    private boolean isPaceSupported;
    private boolean isStepCountSupported;
    private boolean isResistanceLevelSupported;
    private boolean isStrideCountSupported;
    private boolean isExpendedEnergySupported;
    private boolean isHeartRateMeasurementSupported;
    private boolean isMetabolicEquivalentSupported;
    private boolean isElapsedTimeSupported;
    private boolean isRemainingTimeSupported;
    private boolean isPowerMeasurementSupported;
    private boolean isForceOnBeltAndPowerOutputSupported;
    private boolean isUserDataRetentionSupported;
    //Target Setting Features Field
    private boolean isSpeedTargetSettingSupported;
    private boolean isInclinationTargetSettingSupported;
    private boolean isResistanceTargetSettingSupported;
    private boolean isPowerTargetSettingSupported;
    private boolean isHeartRateTargetSettingSupported;
    private boolean isTargetedExpendedEnergyConfigurationSupported;
    private boolean isTargetedStepNumberConfigurationSupported;
    private boolean isTargetedStrideNumberConfigurationSupported;
    private boolean isTargetedDistanceConfigurationSupported;
    private boolean isTargetedTrainingTimeConfigurationSupported;
    private boolean isTargetedTimeInTwoHeartRateZonesConfigurationSupported;
    private boolean isTargetedTimeInThreeHeartRateZonesConfigurationSupported;
    private boolean isTargetedTimeInFiveHeartRateZonesConfigurationSupported;
    private boolean isIndoorBikeSimulationParametersSupported;
    private boolean isWheelCircumferenceConfigurationSupported;
    private boolean isSpinDownControlSupported;
    private boolean isTargetedCadenceConfigurationSupported;

    private byte[] flags = new byte[8];
    private byte[] valueData;

    public static FitnessMachineFeature getInstance()
    {
        data = new FitnessMachineFeature();
        return data;
    }

    public FitnessMachineFeature(){}

    public FitnessMachineFeature(BluetoothGattCharacteristic characteristic){
        byte[] value = characteristic.getValue();
        valueData = value;

        parseData(value);
        for(int i=0;i<value.length;i++){
            Log.i(TAG, "FitnessMachineFeature: i = " + i +"; ox" + Integer.toHexString(BaseUtils.byte1ToInt(value[i])));
        }
    }

    public void parseData(byte[] buffer)
    {
        System.arraycopy(buffer, 0, flags, 0, buffer.length);
        //Fitness Machine Features Field
        isAverageSpeedSupported = ((buffer[0] & 0x01) == 0x01);
        isCadenceSupported = ((buffer[0] & 0x02) == 0x02);
        isTotalDistanceSupported = ((buffer[0] & 0x04) == 0x04);
        isInclinationSupported = ((buffer[0] & 0x08) == 0x08);
        isElevationGainSupported = ((buffer[0] & 0x10) == 0x10);
        isPaceSupported =  ((buffer[0] & 0x20) == 0x20);
        isStepCountSupported = ((buffer[0] & 0x40) == 0x40);
        isResistanceLevelSupported = ((buffer[0] & 0x80) == 0x80);

        isStrideCountSupported = ((buffer[1] & 0x01) == 0x01);
        isExpendedEnergySupported = ((buffer[1] & 0x02) == 0x02);
        isHeartRateMeasurementSupported = ((buffer[1] & 0x04) == 0x04);
        isMetabolicEquivalentSupported = ((buffer[1] & 0x08) == 0x08);
        isElapsedTimeSupported = ((buffer[1] & 0x10) == 0x10);
        isRemainingTimeSupported =  ((buffer[1] & 0x20) == 0x20);
        isPowerMeasurementSupported = ((buffer[1] & 0x40) == 0x40);
        isForceOnBeltAndPowerOutputSupported = ((buffer[1] & 0x80) == 0x80);

        isUserDataRetentionSupported = ((buffer[2] & 0x01) == 0x01);

        //Target Setting Features Field
        isSpeedTargetSettingSupported = ((buffer[4] & 0x01) == 0x01);
        isInclinationTargetSettingSupported = ((buffer[4] & 0x02) == 0x02);
        isResistanceTargetSettingSupported = ((buffer[4] & 0x04) == 0x04);
        isPowerTargetSettingSupported = ((buffer[4] & 0x08) == 0x08);
        isHeartRateTargetSettingSupported = ((buffer[4] & 0x10) == 0x10);
        isTargetedExpendedEnergyConfigurationSupported =  ((buffer[4] & 0x20) == 0x20);
        isTargetedStepNumberConfigurationSupported = ((buffer[4] & 0x40) == 0x40);
        isTargetedStrideNumberConfigurationSupported = ((buffer[4] & 0x80) == 0x80);

        isTargetedDistanceConfigurationSupported = ((buffer[5] & 0x01) == 0x01);
        isTargetedTrainingTimeConfigurationSupported = ((buffer[5] & 0x02) == 0x02);
        isTargetedTimeInTwoHeartRateZonesConfigurationSupported = ((buffer[5] & 0x04) == 0x04);
        isTargetedTimeInThreeHeartRateZonesConfigurationSupported = ((buffer[5] & 0x08) == 0x08);
        isTargetedTimeInFiveHeartRateZonesConfigurationSupported = ((buffer[5] & 0x10) == 0x10);
        isIndoorBikeSimulationParametersSupported =  ((buffer[5] & 0x20) == 0x20);
        isWheelCircumferenceConfigurationSupported = ((buffer[5] & 0x40) == 0x40);
        isSpinDownControlSupported = ((buffer[5] & 0x80) == 0x80);

        isTargetedCadenceConfigurationSupported = ((buffer[6] & 0x01) == 0x01);
    }

    public boolean isAverageSpeedSupported() {
        return isAverageSpeedSupported;
    }

    public boolean isCadenceSupported() {
        return isCadenceSupported;
    }

    public boolean isTotalDistanceSupported() {
        return isTotalDistanceSupported;
    }

    public boolean isInclinationSupported() {
        return isInclinationSupported;
    }

    public boolean isElevationGainSupported() {
        return isElevationGainSupported;
    }

    public boolean isPaceSupported() {
        return isPaceSupported;
    }

    public boolean isStepCountSupported() {
        return isStepCountSupported;
    }

    public boolean isResistanceLevelSupported() {
        return isResistanceLevelSupported;
    }

    public boolean isStrideCountSupported() {
        return isStrideCountSupported;
    }

    public boolean isExpendedEnergySupported() {
        return isExpendedEnergySupported;
    }

    public boolean isHeartRateMeasurementSupported() {
        return isHeartRateMeasurementSupported;
    }

    public boolean isMetabolicEquivalentSupported() {
        return isMetabolicEquivalentSupported;
    }

    public boolean isElapsedTimeSupported() {
        return isElapsedTimeSupported;
    }

    public boolean isRemainingTimeSupported() {
        return isRemainingTimeSupported;
    }

    public boolean isPowerMeasurementSupported() {
        return isPowerMeasurementSupported;
    }

    public boolean isForceOnBeltAndPowerOutputSupported() {
        return isForceOnBeltAndPowerOutputSupported;
    }

    public boolean isUserDataRetentionSupported() {
        return isUserDataRetentionSupported;
    }

    public boolean isSpeedTargetSettingSupported() {
        return isSpeedTargetSettingSupported;
    }

    public boolean isInclinationTargetSettingSupported() {
        return isInclinationTargetSettingSupported;
    }

    public boolean isResistanceTargetSettingSupported() {
        return isResistanceTargetSettingSupported;
    }

    public boolean isPowerTargetSettingSupported() {
        return isPowerTargetSettingSupported;
    }

    public boolean isHeartRateTargetSettingSupported() {
        return isHeartRateTargetSettingSupported;
    }

    public boolean isTargetedExpendedEnergyConfigurationSupported() {
        return isTargetedExpendedEnergyConfigurationSupported;
    }

    public boolean isTargetedStepNumberConfigurationSupported() {
        return isTargetedStepNumberConfigurationSupported;
    }

    public boolean isTargetedStrideNumberConfigurationSupported() {
        return isTargetedStrideNumberConfigurationSupported;
    }

    public boolean isTargetedDistanceConfigurationSupported() {
        return isTargetedDistanceConfigurationSupported;
    }

    public boolean isTargetedTrainingTimeConfigurationSupported() {
        return isTargetedTrainingTimeConfigurationSupported;
    }

    public boolean isTargetedTimeInTwoHeartRateZonesConfigurationSupported() {
        return isTargetedTimeInTwoHeartRateZonesConfigurationSupported;
    }

    public boolean isTargetedTimeInThreeHeartRateZonesConfigurationSupported() {
        return isTargetedTimeInThreeHeartRateZonesConfigurationSupported;
    }

    public boolean isTargetedTimeInFiveHeartRateZonesConfigurationSupported() {
        return isTargetedTimeInFiveHeartRateZonesConfigurationSupported;
    }

    public boolean isIndoorBikeSimulationParametersSupported() {
        return isIndoorBikeSimulationParametersSupported;
    }

    public boolean isWheelCircumferenceConfigurationSupported() {
        return isWheelCircumferenceConfigurationSupported;
    }

    public boolean isSpinDownControlSupported() {
        return isSpinDownControlSupported;
    }

    public boolean isTargetedCadenceConfigurationSupported() {
        return isTargetedCadenceConfigurationSupported;
    }

    @Override
    public String convert2String() {
        String data = "";
        if (isAverageSpeedSupported()){
            data += "Average Speed Supported\n";
        }
        if(isCadenceSupported()){
            data += "Cadence Supported\n";
        }
        if(isTotalDistanceSupported()){
            data += "Total Distance Supported\n";
        }
        if(isInclinationSupported()){
            data += "Inclination Supported\n";
        }
        if (isElevationGainSupported()){
            data += "Elevation Gain Supported\n";
        }
        if(isPaceSupported()){
            data += "Pace  Supported\n";
        }
        if(isStepCountSupported()){
            data += "Step Count Supported\n";
        }
        if(isResistanceLevelSupported()){
            data += "Resistance Level Supported\n";
        }
        if (isStrideCountSupported()){
            data += "Stride Count Supported\n";
        }
        if(isExpendedEnergySupported()){
            data += "Expended Energy Supported\n";
        }
        if(isHeartRateMeasurementSupported()){
            data += "HeartRate Measurement Supported\n";
        }
        if(isMetabolicEquivalentSupported()){
            data += "Metabolic Equivalent Supported\n";
        }
        if (isElapsedTimeSupported()){
            data += "Elapsed Time Supported\n";
        }
        if(isRemainingTimeSupported()){
            data += "Remaining Time Supported\n";
        }
        if(isPowerMeasurementSupported()){
            data += "Power Measurement Supported\n";
        }
        if(isForceOnBeltAndPowerOutputSupported()){
            data += "Force On Belt And Power Output Supported\n";
        }
        if (isUserDataRetentionSupported()){
            data += "User Data Retention Supported\n";
        }
        if(isSpeedTargetSettingSupported()){
            data += "Speed Target Setting Supported\n";
        }
        if(isInclinationTargetSettingSupported()){
            data += "Inclination Target Setting Supported\n";
        }
        if(isResistanceTargetSettingSupported()){
            data += "Resistance Target Setting Supported\n";
        }
        if (isPowerTargetSettingSupported()){
            data += "Power Target Setting Supported\n";
        }
        if(isHeartRateTargetSettingSupported()){
            data += "Heart Rate Target Setting Supported\n";
        }
        if(isTargetedExpendedEnergyConfigurationSupported()){
            data += "Targeted Expended Energy Configuration Supported\n";
        }
        if(isTargetedStepNumberConfigurationSupported()){
            data += "Targeted Step Number Configuration Supported\n";
        }
        if (isTargetedStrideNumberConfigurationSupported()){
            data += "Targeted Stride Number Configuration Supported\n";
        }
        if (isTargetedDistanceConfigurationSupported()){
            data += "Targeted Distance Configuration Supported\n";
        }
        if(isTargetedTrainingTimeConfigurationSupported()){
            data += "Targeted Training Time Configuration Supported\n";
        }
        if(isTargetedTimeInTwoHeartRateZonesConfigurationSupported()){
            data += "Targeted Time In Two HeartRate Zones Configuration Supported\n";
        }
        if(isTargetedTimeInThreeHeartRateZonesConfigurationSupported()){
            data += "Targeted Time In Three HeartRate Zones Configuration Supported\n";
        }
        if (isTargetedTimeInFiveHeartRateZonesConfigurationSupported()){
            data += "Targeted Time In Five HeartRate Zones Configuration Supported\n";
        }
        if (isIndoorBikeSimulationParametersSupported()){
            data += "IndoorBike Simulation Parameters Supported\n";
        }
        if(isWheelCircumferenceConfigurationSupported()){
            data += "Wheel Circumference Configuration Supported\n";
        }
        if(isSpinDownControlSupported()){
            data += "SpinDown Control Supported\n";
        }
        if(isTargetedCadenceConfigurationSupported()){
            data += "Targeted Cadence Configuration Supported\n";
        }

        return data;
    }

    @Override
    public String merge(TranslateData translateData) {
        return null;
    }

    @Override
    public boolean hasMoreData() {
        return false;
    }

    @Override
    public byte[] getData() {
        return valueData;
    }

    @Override
    public boolean isInvalidData() {
        return false;
    }
}
