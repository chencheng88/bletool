package com.jht.bletool.characteristic.userdata;


import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;


import androidx.annotation.Keep;

import com.jht.bletool.characteristic.TranslateData;
import com.jht.bletool.util.BaseUtils;

import java.io.UnsupportedEncodingException;

import top.codestudy.annotation_uuid.MyUUID;

@Keep
@MyUUID(uuid = "00002a85-0000-1000-8000-00805f9b34fb")
public class DateofBirth implements TranslateData {

    private static final String TAG = "DateofBirth";
    private String year = "";
    private String month = "";
    private String day = "";
    private byte[] value;

    public DateofBirth() {
    }
    public DateofBirth(BluetoothGattCharacteristic characteristic) {
        value = characteristic.getValue();
        int i = BaseUtils.bytes2ToInt(value,0);
        year += i;
        if (i == 0){
            year =  "Year is not known";
        }
        int i2 = BaseUtils.byte1ToInt(value,2);
        switch (i2){
            case 0:
                month += "Month is not known";
                break;
            case 1:
                month += "January";
                break;
            case 2:
                month += "February";
                break;
            case 3:
                month += "March";
                break;
            case 4:
                month += "April";
                break;
            case 5:
                month += "May";
                break;
            case 6:
                month += "June";
                break;
            case 7:
                month += "July";
                break;
            case 8:
                month += "August";
                break;
            case 9:
                month += "September";
                break;
            case 10:
                month += "October";
                break;
            case 11:
                month += "November";
                break;
            case 12:
                month += "December";
                break;
        }
        int i3 = BaseUtils.byte1ToInt(value,3);
        day += i3;
        if (i3 == 0 ){
            day = "Day of Month is not known";
        }
    }

    @Override
    public String convert2String() {
        return "Year: " + year +" ; Month: " + month +" ; Day: " + day+" ;"+"\n";
    }

    @Override
    public String merge(TranslateData translateData) {
        return null;
    }
    @Override
    public boolean hasMoreData() {
        return false;
    }

    @Override
    public byte[] getData() {
        return value;
    }


    @Override
    public boolean isInvalidData() {
        return false;
    }
}
