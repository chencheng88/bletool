package com.jht.bletool.characteristic.userdata;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import androidx.annotation.Keep;

import top.codestudy.annotation_uuid.MyUUID;
import com.jht.bletool.characteristic.TranslateData;
import com.jht.bletool.util.BaseUtils;

import java.io.UnsupportedEncodingException;

@Keep
@MyUUID(uuid = "00002a8c-0000-1000-8000-00805f9b34fb")
public class Gender implements TranslateData {
    private static final String TAG = "Gender";
    private String gender = "";
    private byte[] value;

    public Gender() {
    }

    public Gender(BluetoothGattCharacteristic characteristic) {
        value = characteristic.getValue();
        int i = BaseUtils.byte1ToInt(value[0]);
        switch (i){
            case 0:
                gender += "Male";
                break;
            case 1:
                gender += "Female";
            case 2:
                gender += "Unspecified";
                break;
            default:
                gender += "don't have this field";
                break;
        }

    }

    @Override
    public String convert2String() {
        return "Gender: " + gender+"\n";
    }

    @Override
    public String merge(TranslateData translateData) {
        return null;
    }
    @Override
    public boolean hasMoreData() {
        return false;
    }

    @Override
    public byte[] getData() {
        return value;
    }

    @Override
    public boolean isInvalidData() {
        return false;
    }
}
