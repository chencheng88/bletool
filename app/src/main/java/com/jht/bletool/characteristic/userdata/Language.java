package com.jht.bletool.characteristic.userdata;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import androidx.annotation.Keep;

import top.codestudy.annotation_uuid.MyUUID;
import com.jht.bletool.characteristic.TranslateData;

import java.io.UnsupportedEncodingException;

@Keep
@MyUUID(uuid = "00002aa2-0000-1000-8000-00805f9b34fb")
public class Language implements TranslateData {
    private static final String TAG = "Language";
    private String language = "";
    private byte[] value;
    public Language() {
    }

    public Language(BluetoothGattCharacteristic characteristic) {
        value = characteristic.getValue();
        try {
            language += new String(value, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            Log.e(TAG, "Language: error");
        }
    }

    @Override
    public String convert2String() {
        return "Language: " + language+"\n";
    }


    @Override
    public String merge(TranslateData translateData) {
        return null;
    }

    @Override
    public boolean hasMoreData() {
        return false;
    }

    @Override
    public byte[] getData() {
        return value;
    }

    @Override
    public boolean isInvalidData() {
        return false;
    }
}
