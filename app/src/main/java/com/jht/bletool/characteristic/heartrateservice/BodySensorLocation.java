package com.jht.bletool.characteristic.heartrateservice;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import androidx.annotation.Keep;

import top.codestudy.annotation_uuid.MyUUID;
import com.jht.bletool.characteristic.TranslateData;

@Keep
@MyUUID(uuid="00002a38-0000-1000-8000-00805f9b34fb")
public class BodySensorLocation implements TranslateData {

    private static final String TAG = "BodySensorLocation";

    private String  bodySensorLocation = "";
    private byte[] value;

    public BodySensorLocation(){

    }

    public BodySensorLocation(BluetoothGattCharacteristic characteristic){
        value = characteristic.getValue();

        switch (value[0]){
            case 0:
                bodySensorLocation = "Other";
                break;
            case 1:
                bodySensorLocation = "Chest";
                break;
            case 2:
                bodySensorLocation = "Wrist";
                break;
            case 3:
                bodySensorLocation = "Finger";
                break;
            case 4:
                bodySensorLocation = "Hand";
                break;
            case 5:
                bodySensorLocation = "Ear Lobe";
                break;
            case 6:
                bodySensorLocation = "Foot";
                break;
            default:
                bodySensorLocation = "";
                break;
        }
        Log.i(TAG, "BodySensorLocation: " + bodySensorLocation);
    }

    @Override
    public String convert2String() {
        return "The wearing position of the equipment is the " + bodySensorLocation+"\n";
    }

    @Override
    public String merge(TranslateData translateData) {
        return null;
    }
    @Override
    public boolean hasMoreData() {
        return false;
    }

    @Override
    public byte[] getData() {
        return value;
    }

    @Override
    public boolean isInvalidData() {
        return false;
    }
}
