package com.jht.bletool.characteristic.ftms;


import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import androidx.annotation.Keep;

import top.codestudy.annotation_uuid.MyUUID;
import com.jht.bletool.characteristic.TranslateData;
import com.jht.bletool.util.BaseUtils;

@Keep
@MyUUID(uuid = "00002ad8-0000-1000-8000-00805f9b34fb")
public class SupportedPowerRange implements TranslateData {
    private final String TAG = "SupportedPowerRange";
    private static SupportedPowerRange data;

    private byte[] MinimumPower = new byte[2];
    private byte[] MaximumPower = new byte[2];
    private byte[] MinimumIncrement = new byte[2];
    private byte[] valueData;

    public static SupportedPowerRange getInstance() {
        data = new SupportedPowerRange();
        return data;
    }

    public SupportedPowerRange() {
    }
    public SupportedPowerRange(BluetoothGattCharacteristic characteristic){
        byte[] value = characteristic.getValue();
        valueData = value;
        parseData(value);
        for(int i=0;i<value.length;i++){
            Log.i(TAG, "SupportedPowerRange: i = " + i +"; ox" + Integer.toHexString(BaseUtils.byte1ToInt(value[i])));
        }
    }
    public void parseData(byte[] buffer) {
        System.arraycopy(buffer, 0, MinimumPower, 0, 2);
        System.arraycopy(buffer, 2, MaximumPower, 0, 2);
        System.arraycopy(buffer, 4, MinimumIncrement, 0, 2);
    }

    public int getMinimumPower() {
        return BaseUtils.bytes2ToInt(MinimumPower,0) ;
    }

    public int getMaximumPower() {
        return BaseUtils.bytes2ToInt(MaximumPower,0) ;
    }

    public int getMinimumIncrement() {
        return BaseUtils.bytes2ToInt(MinimumIncrement,0);
    }

    @Override
    public String convert2String() {
        return "SupportedPowerRange:\nMinimumPower : " +  getMinimumPower()
                + " watt \n MaximumPower : " +   getMaximumPower()
                + " watt \n MinimumIncrement : " +  getMinimumIncrement() + " watt "+"\n";
    }

    @Override
    public String merge(TranslateData translateData) {
        return null;
    }
    @Override
    public boolean hasMoreData() {
        return false;
    }

    @Override
    public byte[] getData() {
        return valueData;
    }

    @Override
    public boolean isInvalidData() {
        return false;
    }
}
