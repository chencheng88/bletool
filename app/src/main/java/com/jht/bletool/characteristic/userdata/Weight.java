package com.jht.bletool.characteristic.userdata;

import android.bluetooth.BluetoothGattCharacteristic;

import androidx.annotation.Keep;

import top.codestudy.annotation_uuid.MyUUID;
import com.jht.bletool.characteristic.TranslateData;
import com.jht.bletool.util.BaseUtils;

import java.math.BigDecimal;

@Keep
@MyUUID(uuid = "00002a98-0000-1000-8000-00805f9b34fb")
public class Weight implements TranslateData {
    private static final String TAG = "Weight";
    private double weight ;
    private byte[] value;

    public Weight(){}

    public Weight(BluetoothGattCharacteristic characteristic) {
        value = characteristic.getValue();
        int i = BaseUtils.bytes2ToInt(value,0);
        BigDecimal bigInt = new BigDecimal(i);
        BigDecimal bigDouble = new BigDecimal("0.005");
        weight =  bigInt.multiply(bigDouble).doubleValue();
    }

    @Override
    public String convert2String() {
        return "Weight: " + weight  + " kg"+"\n";
    }

    @Override
    public String merge(TranslateData translateData) {
        return null;
    }

    @Override
    public boolean hasMoreData() {
        return false;
    }

    @Override
    public byte[] getData() {
        return value;
    }

    @Override
    public boolean isInvalidData() {
        return false;
    }
}
