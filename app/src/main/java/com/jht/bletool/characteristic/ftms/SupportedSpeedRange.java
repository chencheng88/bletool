package com.jht.bletool.characteristic.ftms;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import androidx.annotation.Keep;

import top.codestudy.annotation_uuid.MyUUID;
import com.jht.bletool.characteristic.TranslateData;
import com.jht.bletool.util.BaseUtils;

import java.math.BigDecimal;

@Keep
@MyUUID(uuid = "00002ad4-0000-1000-8000-00805f9b34fb")
public class SupportedSpeedRange implements TranslateData {
    private final String TAG = "SupportedSpeedRange";
    private static SupportedSpeedRange data;

    private byte[] MinimumSpeed = new byte[2];
    private byte[] MaximumSpeed = new byte[2];;
    private byte[] MinimumIncrement = new byte[2];;
    private byte[] valueData;
    public static SupportedSpeedRange getInstance()
    {
        data = new SupportedSpeedRange();
        return data;
    }
    public SupportedSpeedRange(){}
    public SupportedSpeedRange(BluetoothGattCharacteristic characteristic){
        byte[] value = characteristic.getValue();
        valueData = value;
        parseData(value);
        for(int i=0;i<value.length;i++){
            Log.i(TAG, "SupportedSpeedRange: i = " + i +"; ox" + Integer.toHexString(BaseUtils.byte1ToInt(value[i])));
        }
    }
    public void parseData(byte[] buffer)
    {
        System.arraycopy(buffer, 0, MinimumSpeed, 0, 2);
        System.arraycopy(buffer, 2, MaximumSpeed, 0, 2);
        System.arraycopy(buffer, 4, MinimumIncrement, 0, 2);
    }

    public double getMinimumSpeed() {
        Log.i(TAG, "bytes2ToInt: MinimumSpeed ==> 0x" + Integer.toHexString(MinimumSpeed[0]) + "-" + Integer.toHexString(MinimumSpeed[1]) );
        BigDecimal bigInt = new BigDecimal(BaseUtils.bytes2ToInt(MinimumSpeed,0));
        BigDecimal bigDouble = new BigDecimal("0.01");
        return bigInt.multiply(bigDouble).doubleValue();
    }

    public double getMaximumSpeed() {
        Log.i(TAG, "bytes2ToInt: MaximumSpeed ==> 0x" + Integer.toHexString(MaximumSpeed[0]) +"-" +  Integer.toHexString(MaximumSpeed[1]) );
        BigDecimal bigInt = new BigDecimal(BaseUtils.bytes2ToInt(MaximumSpeed,0));
        BigDecimal bigDouble = new BigDecimal("0.01");
        return bigInt.multiply(bigDouble).doubleValue();
    }

    public double getMinimumIncrement() {
        Log.i(TAG, "bytes2ToInt: MinimumIncrement ==> 0x" + Integer.toHexString(MinimumIncrement[0]) + "-" + Integer.toHexString(MinimumIncrement[1]) );
        BigDecimal bigInt = new BigDecimal(BaseUtils.bytes2ToInt(MinimumIncrement,0));
        BigDecimal bigDouble = new BigDecimal("0.01");
        return bigInt.multiply(bigDouble).doubleValue();
    }

    @Override
    public String convert2String() {
        return "SupportedSpeedRange:\nMinimumSpeed : " +  getMinimumSpeed()
                + " km/h \n MaximumSpeed : " +  getMaximumSpeed()
                + " km/h \n MinimumIncrement : " +  getMinimumIncrement() + " km/h "+"\n";
    }

    @Override
    public String merge(TranslateData translateData) {
        return null;
    }

    @Override
    public boolean hasMoreData() {
        return false;
    }

    @Override
    public byte[] getData() {
        return valueData;
    }

    @Override
    public boolean isInvalidData() {
        return false;
    }
}
