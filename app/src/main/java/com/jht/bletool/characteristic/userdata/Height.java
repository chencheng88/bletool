package com.jht.bletool.characteristic.userdata;

import android.bluetooth.BluetoothGattCharacteristic;

import androidx.annotation.Keep;

import top.codestudy.annotation_uuid.MyUUID;
import com.jht.bletool.characteristic.TranslateData;
import com.jht.bletool.util.BaseUtils;

@Keep
@MyUUID(uuid = "00002a8e-0000-1000-8000-00805f9b34fb")
public class Height implements TranslateData {
    private static final String TAG = "Height";
    private String height = "";
    private byte[] value;

    public Height() {
    }

    public Height(BluetoothGattCharacteristic characteristic) {
        value = characteristic.getValue();
        int i = BaseUtils.bytes2ToInt(value,0);
        height += i;
    }

    @Override
    public String convert2String() {
        return "Height: " + height + " centimeter;"+"\n";
    }

    @Override
    public String merge(TranslateData translateData) {
        return null;
    }
    @Override
    public boolean hasMoreData() {
        return false;
    }

    @Override
    public byte[] getData() {
        return value;
    }

    @Override
    public boolean isInvalidData() {
        return false;
    }
}
