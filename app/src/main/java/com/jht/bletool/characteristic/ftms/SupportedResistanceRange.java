package com.jht.bletool.characteristic.ftms;


import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import androidx.annotation.Keep;

import top.codestudy.annotation_uuid.MyUUID;
import com.jht.bletool.characteristic.TranslateData;
import com.jht.bletool.util.BaseUtils;
import com.jht.bletool.util.ComputerUtil;

@Keep
@MyUUID(uuid = "00002ad6-0000-1000-8000-00805f9b34fb")
public class SupportedResistanceRange implements TranslateData {


    private final String TAG = "SupportedResistanceRange";
    private static SupportedResistanceRange data;

    private byte[] MinimumResistanceLevel = new byte[2];
    private byte[] MaximumResistanceLevel = new byte[2];
    private byte[] MinimumIncrement = new byte[2];
    private byte[] valueData;
    public static SupportedResistanceRange getInstance() {
        data = new SupportedResistanceRange();
        return data;
    }

    public SupportedResistanceRange() {
    }
    public SupportedResistanceRange(BluetoothGattCharacteristic characteristic){
        byte[] value = characteristic.getValue();
        valueData =value;
        parseData(value);
        for(int i=0;i<value.length;i++){
            Log.i(TAG, "SupportedResistanceRange: i = " + i +"; ox" + Integer.toHexString(BaseUtils.byte1ToInt(value[i])));
        }
    }
    public void parseData(byte[] buffer) {
        System.arraycopy(buffer, 0, MinimumResistanceLevel, 0, 2);
        System.arraycopy(buffer, 2, MaximumResistanceLevel, 0, 2);
        System.arraycopy(buffer, 4, MinimumIncrement, 0, 2);
    }

    public double getMinimumResistanceLevel() {
        return ComputerUtil.multiply(BaseUtils.bytes2ToInt(MinimumResistanceLevel,0),"0.1");
    }

    public double getMaximumResistanceLevel() {
        return ComputerUtil.multiply(BaseUtils.bytes2ToInt(MaximumResistanceLevel,0),"0.1");
    }

    public double getMinimumIncrement() {
        return ComputerUtil.multiply(BaseUtils.bytes2ToInt(MinimumIncrement,0),"0.1");
    }

    @Override
    public String convert2String() {
        return "SupportedResistanceRange:\nMinimumResistanceLevel : " +  getMinimumResistanceLevel()
                + " \n MaximumResistanceLevel : " +  getMaximumResistanceLevel()
                + " \n MinimumIncrement : " +  getMinimumIncrement() + " "+"\n";
    }

    @Override
    public String merge(TranslateData translateData) {
        return null;
    }
    @Override
    public boolean hasMoreData() {
        return false;
    }

    @Override
    public byte[] getData() {

        return valueData;
    }

    @Override
    public boolean isInvalidData() {
        return false;
    }
}
