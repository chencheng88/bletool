package com.jht.bletool.characteristic.deviceinfromations;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import androidx.annotation.Keep;

import top.codestudy.annotation_uuid.MyUUID;
import com.jht.bletool.characteristic.TranslateData;

import java.io.UnsupportedEncodingException;

@Keep
@MyUUID(uuid = "00002a25-0000-1000-8000-00805f9b34fb")
public class SerialNumberString implements TranslateData {
    private static final String TAG = "SerialNumberString";
    private String serialNumberString = "";
    private byte[] value;
    public SerialNumberString() {
    }

    public SerialNumberString(BluetoothGattCharacteristic characteristic) {
        value = characteristic.getValue();
        try {
            serialNumberString += new String(value, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            Log.e(TAG, "SerialNumberString: error");
        }
    }
    @Override
    public String convert2String() {
        return "Serial Number: " + serialNumberString+"\n";
    }

    @Override
    public String merge(TranslateData translateData) {
        return null;
    }

    @Override
    public boolean hasMoreData() {
        return false;
    }

    @Override
    public byte[] getData() {
        return value;
    }

    @Override
    public boolean isInvalidData() {
        return false;
    }
}
