package com.jht.bletool.characteristic.deviceinfromations;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import androidx.annotation.Keep;

import top.codestudy.annotation_uuid.MyUUID;
import com.jht.bletool.characteristic.TranslateData;

import java.io.UnsupportedEncodingException;

@Keep
@MyUUID(uuid = "00002a26-0000-1000-8000-00805f9b34fb")
public class FirmwareRevisionString implements TranslateData {
    private static final String TAG = "FirmwareRevisionString";
    private String firmwareRevisionString = "";
    private byte[] value;

    public FirmwareRevisionString() {
    }

    public FirmwareRevisionString(BluetoothGattCharacteristic characteristic) {
        value = characteristic.getValue();
        try {
            firmwareRevisionString += new String(value, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            Log.e(TAG, "firmwareRevisionString: error");
        }
    }

    @Override
    public String convert2String() {

        return "Firmware Revision: " + firmwareRevisionString +"\n";
    }

    @Override
    public String merge(TranslateData translateData) {
        return null;
    }
    @Override
    public boolean hasMoreData() {
        return false;
    }

    @Override
    public byte[] getData() {
        return value;
    }

    @Override
    public boolean isInvalidData() {
        return false;
    }
}
