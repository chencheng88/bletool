package com.jht.bletool.characteristic;

public interface TranslateData {

    String convert2String();

    String merge(TranslateData translateData);

    boolean hasMoreData();

    /**
     *
     * @return 返回的数据可能为null，因此需要特殊处理
     */
    byte[] getData();

    /**
     * 是否是无效数据
     */
    boolean isInvalidData();

}
