package com.jht.bletool.characteristic.deviceinfromations;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import androidx.annotation.Keep;

import top.codestudy.annotation_uuid.MyUUID;
import com.jht.bletool.characteristic.TranslateData;
import com.jht.bletool.util.BaseUtils;

@Keep
@MyUUID(uuid = "00002a23-0000-1000-8000-00805f9b34fb")
public class SystemID implements TranslateData {
    private static final String TAG = "SystemID";
    private String systemID = "";
    private String manufacturerIdentifier = "";
    private String organizationallyUniqueIdentifier = "";
    private byte[] value;
    public SystemID() {
    }

    public SystemID(BluetoothGattCharacteristic characteristic) {
        value = characteristic.getValue();
        long mManufacturerIdentifier = BaseUtils.byte5ToIntR(value, 0);
        int mOrganizationallyUniqueIdentifier = BaseUtils.byte3ToIntR(value, 5);
        Log.i(TAG, "SystemID: mManufacturerIdentifier ==> " + mManufacturerIdentifier + " ; mOrganizationallyUniqueIdentifier ==> " + mOrganizationallyUniqueIdentifier);
        manufacturerIdentifier += "0x";
        String mManufacturerIdentifierStr = Long.toHexString(mManufacturerIdentifier);
        if(mManufacturerIdentifierStr.length()<=10){
            for(int i=0;i<10-mManufacturerIdentifierStr.length();i++){
                manufacturerIdentifier += "0";
            }
        }
        manufacturerIdentifier += mManufacturerIdentifierStr;

        organizationallyUniqueIdentifier += "0x";
        String organizationallyUniqueIdentifierStr = Integer.toHexString(mOrganizationallyUniqueIdentifier);
        if(organizationallyUniqueIdentifierStr.length()<=10){
            for(int i=0;i<6-organizationallyUniqueIdentifierStr.length();i++){
                organizationallyUniqueIdentifier += "0";
            }
        }
        organizationallyUniqueIdentifier += organizationallyUniqueIdentifierStr;
    }

    @Override
    public String convert2String() {
        return "Manufacturer Identifier: " + manufacturerIdentifier +"\n ; organizationallyUniqueIdentifier: " + organizationallyUniqueIdentifier+"\n";
    }

    @Override
    public String merge(TranslateData translateData) {
        return null;
    }

    @Override
    public boolean hasMoreData() {
        return false;
    }

    @Override
    public byte[] getData() {
        return value;
    }

    @Override
    public boolean isInvalidData() {
        return false;
    }
}
