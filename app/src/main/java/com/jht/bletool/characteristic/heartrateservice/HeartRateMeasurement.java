package com.jht.bletool.characteristic.heartrateservice;


import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import androidx.annotation.Keep;

import top.codestudy.annotation_uuid.MyUUID;
import com.jht.bletool.characteristic.TranslateData;
import com.jht.bletool.util.BaseUtils;

@Keep
@MyUUID(uuid="00002a37-0000-1000-8000-00805f9b34fb")
public class HeartRateMeasurement implements TranslateData {
    private static final String TAG = "HeartRateMeasurement";
    private int heart;
    private byte[] value;
    public HeartRateMeasurement() {
    }

    public HeartRateMeasurement(BluetoothGattCharacteristic characteristic) {
        int flag = characteristic.getProperties();
        int format = -1;
        if ((flag & 0x01) != 0) {
            format = BluetoothGattCharacteristic.FORMAT_UINT16;

        } else {
            format = BluetoothGattCharacteristic.FORMAT_UINT8;

        }
        value = characteristic.getValue();
        if (format == BluetoothGattCharacteristic.FORMAT_UINT16){
            Log.i(TAG, "HeartRateMeasurement: heartRate ==> " + BaseUtils.bytes2ToInt(value,1));
        }
        if (format == BluetoothGattCharacteristic.FORMAT_UINT8){
            Log.i(TAG, "HeartRateMeasurement: heartRate ==> " + BaseUtils.byte1ToInt(value,1));
        }

        final int heartRate = characteristic.getIntValue(format, 1);

        this.heart = heartRate;
    }

    @Override
    public String convert2String() {
        return "heart rate is " + heart + " bpm"+"\n";
    }

    @Override
    public String merge(TranslateData translateData) {
        return null;
    }
    @Override
    public boolean hasMoreData() {
        return false;
    }

    @Override
    public byte[] getData() {
        return value;
    }

    @Override
    public boolean isInvalidData() {
        return false;
    }
}
