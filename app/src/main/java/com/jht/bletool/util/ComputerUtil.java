package com.jht.bletool.util;

import java.math.BigDecimal;

public final class ComputerUtil {

    public static double multiply(int val,String strDouble) {

        BigDecimal bigInt = new BigDecimal(val);
        BigDecimal bigDouble = new BigDecimal(strDouble);
        return bigInt.multiply(bigDouble).doubleValue();
    }
}
