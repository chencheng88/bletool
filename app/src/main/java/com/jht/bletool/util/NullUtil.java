package com.jht.bletool.util;

public class NullUtil {

    public static void checkObjectNull(Object obj) {
        if (obj == null) {
            throw new RuntimeException(obj + " is null !!");
        }
    }

}
