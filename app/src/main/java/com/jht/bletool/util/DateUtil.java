package com.jht.bletool.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    public static  String getCurrentTime(){
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss.SSS");
        String format = simpleDateFormat.format(date);
        return format+"> ";
    }

}
