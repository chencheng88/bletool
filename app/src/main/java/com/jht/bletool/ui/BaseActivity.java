package com.jht.bletool.ui;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Window;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.jht.bletool.R;

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarColor(R.color.light_blue3);

    }

    public void setStatusBarColor(int colorId) {
        Window window = getWindow();
        window.setStatusBarColor(Color.parseColor("#0082FC"));
    }

}
