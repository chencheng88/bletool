package com.jht.bletool.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import com.jht.bletool.R;
import com.jht.bletool.fragment.MySettingsFragment;

public class AppSettingActivity extends BaseActivity implements PreferenceFragmentCompat.OnPreferenceStartFragmentCallback {
    private static final String TAG = "AppSettingActivity";
    private ImageView mIvMenu;
    private TextView mTvTitle;
    private FrameLayout mFlSetting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_setting);
        initView();
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        MySettingsFragment mySettingsFragment = new MySettingsFragment();
        supportFragmentManager.beginTransaction().replace(R.id.fl_setting, mySettingsFragment).commit();
    }

    private void initView() {
        mIvMenu = findViewById(R.id.iv_menu);
        mIvMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mTvTitle = findViewById(R.id.tv_title);
        mFlSetting = findViewById(R.id.fl_setting);
    }

    @Override
    public boolean onPreferenceStartFragment(PreferenceFragmentCompat caller, Preference pref) {
        caller.toString();
        Log.i(TAG, "onPreferenceStartFragment: Key = " + pref.getKey() + " ; Summary = " + pref.getSummary());
        return false;
    }
}