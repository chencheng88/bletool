package com.jht.bletool.ui;

import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.jht.bletool.R;
import com.jht.bletool.entity.Version;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class VersionActivity extends BaseActivity {
    private static final String TAG = "VersionActivity";
    private ImageView mIvVersionMenu;
    private TextView mTvVersion;
    private FrameLayout mFlSetting;

    private TextView mTvVersionShow;


    Handler handler = new Handler();
    private volatile List<Version> versions = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_version);

        mIvVersionMenu = findViewById(R.id.iv_version_menu);
        mTvVersion = findViewById(R.id.tv_version);
        mIvVersionMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mTvVersionShow = findViewById(R.id.tv_version_show);

        new Thread(new Runnable() {

            @Override
            public void run() {
                //加载Version信息到布局
                InputStream inputStream = getResources().openRawResource(R.raw.version);
                try {
                    StringBuilder sb = new StringBuilder();
                    XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser parser = factory.newPullParser();
                    parser.setInput(inputStream,"UTF-8");
                    Version version = null;
                    boolean liStart = false;
                    int eventType = parser.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        switch (eventType) {
                            case XmlPullParser.START_TAG://开始解析
                                String nodeName = parser.getName();
                                Log.d(TAG, "nodeName: "+nodeName);
                                if ("version".equals(nodeName)) {
                                    version = new Version();
                                    String name = parser.getAttributeValue(null, "name");
                                    String code = parser.getAttributeValue(null, "code");
                                    version.setVersionName(name );
                                    version.setVersionCode(code );
                                    Log.d(TAG, "run: code =" + code + " ; name = " + name);
                                }
                                if ("li".equals(nodeName)) {
                                    liStart = true;
                                }
                                break;
                            case XmlPullParser.TEXT:
                                String liStr = parser.getText();
                                if (liStart && liStr != null && !"".equals(liStr)){
                                    version.getItem().add(liStr);
                                }
                                break;
                            case XmlPullParser.END_TAG://完成解析
                                String endNodeName = parser.getName();
                                if ("version".equals(endNodeName)) {
                                    Log.e(TAG, "run: version end");
                                    versions.add(version);
                                }
                                if ("li".equals(endNodeName)){
                                    liStart = false;
                                }
                                break;
                            default:
                                break;
                        }
                        eventType=parser.next();
                    }
                    for(Version v :versions){
                        sb.append(v.toString());
                        sb.append("<br/>");
                        sb.append("<hr/>");
                    }
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            mTvVersionShow.setText(Html.fromHtml(sb.toString()));
                        }
                    });
                } catch (XmlPullParserException | IOException e) {
                    e.printStackTrace();
                }

            }
        }).start();

    }
}