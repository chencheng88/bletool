package com.jht.bletool.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.jht.bletool.BuildConfig;
import com.jht.bletool.R;
import com.jht.bletool.config.VersionChangeLog;

import top.codestudy.lib_common_ui.splash.SplashActivity;

public class SplashMainActivity extends AppCompatActivity {

    private static final String TAG = "SplashMainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_splash);

        SharedPreferences sharedPref = getSharedPreferences(
                "com.jht.bletool", Context.MODE_PRIVATE);
        int version_code = sharedPref.getInt("version_code", 0);

        if (version_code != BuildConfig.VERSION_CODE) {
            SharedPreferences.Editor edit = sharedPref.edit();
            edit.putInt("version_code", BuildConfig.VERSION_CODE);
            edit.apply();
            Intent intent = new Intent(this, SplashActivity.class);
            intent.putExtra("text_title", VersionChangeLog.titles);
            intent.putExtra("text_content", VersionChangeLog.contents);
            intent.putExtra("imgs", VersionChangeLog.imgs);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivityForResult(intent, SplashActivity.RESULT_CODE);
                }
            },1000);
        }else{
            Intent intent = new Intent(SplashMainActivity.this, MainActivity.class);
            startActivity(intent);
            finish();

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == SplashActivity.RESULT_CODE) {
            Intent intent = new Intent(SplashMainActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }

    }
}