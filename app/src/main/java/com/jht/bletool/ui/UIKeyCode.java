package com.jht.bletool.ui;

public interface UIKeyCode {

    int SCAN_STATE_INIT = 0;
    int SCAN_STATE_START = 1;
    int SCAN_STATE_STOP = 2;

    //MainActivity uiHandler
    int UI_SHOW_GO_SETTING_DIALOG = 100;
    int UI_STOP_ROTATION = 101;
    int UI_START_ROTATION = 102;
     //fragment BLEService
    int UI_CLOSE_PROGRESS = 200;
}
