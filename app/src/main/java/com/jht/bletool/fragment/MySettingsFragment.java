package com.jht.bletool.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import androidx.fragment.app.FragmentActivity;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;

import com.jht.bletool.APP;
import com.jht.bletool.BuildConfig;
import com.jht.bletool.R;
import com.jht.bletool.config.AppConfig;
import com.jht.bletool.config.VersionChangeLog;
import com.jht.bletool.ui.VersionActivity;

import top.codestudy.lib_common_ui.splash.SplashActivity;

public class MySettingsFragment extends PreferenceFragmentCompat {
    private static final String TAG = "MySettingsFragment";
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {

        setPreferencesFromResource(R.xml.preferences, rootKey);
        initScanTime();
        initVersion();
        initVersionChange();
    }

    private void initScanTime() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        final String scan_time = sharedPreferences.getString("scan_time", "20000");
        Log.i(TAG, "onCreatePreferences: scan_time == >" + scan_time);
        ListPreference scanTime = findPreference("scan_time");
        //设置初始值
        final ListPreferenceSummaryProvider summaryProvider = new ListPreferenceSummaryProvider("当前扫描时间是"+ scan_time.substring(0,2) +"秒");
        scanTime.setSummaryProvider(summaryProvider);

        scanTime.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                Log.i(TAG, "onPreferenceChange: newValue => " + newValue);
                summaryProvider.setMsg("当前扫描时间是"+ ((String)newValue.toString()).substring(0,2) +"秒");
                AppConfig.init(Long.parseLong(newValue.toString(),10));
                return true;
            }
        });
    }

    private void initVersion() {
        Preference version_name = findPreference("version_name");
        version_name.setSummaryProvider(new Preference.SummaryProvider<Preference>() {
            @Override
            public CharSequence provideSummary(Preference preference) {
                PackageManager pm = APP.getAppContext().getPackageManager();
                PackageInfo pi = null;
                try {
                    pi = pm.getPackageInfo( APP.getAppContext().getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                    return "null";
                }
                return  pi.versionName+"_"+BuildConfig.type;
            }
        });
        Intent intent = new Intent(APP.getAppContext(), VersionActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        version_name.setIntent(intent);
    }

    private void initVersionChange() {
        Preference version_change = findPreference("change_log");
        Intent intent = new Intent(APP.getAppContext(), SplashActivity.class);
        intent.putExtra("text_title", VersionChangeLog.titles);
        intent.putExtra("text_content",VersionChangeLog.contents);
        intent.putExtra("imgs",VersionChangeLog.imgs);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        version_change.setIntent(intent);
    }

    private class ListPreferenceSummaryProvider implements Preference.SummaryProvider<ListPreference>{
        private String msg;

        public ListPreferenceSummaryProvider(String msg){
            this.msg = msg;
        }

        @Override
        public CharSequence provideSummary(ListPreference preference) {
            return msg;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }
    }
}
