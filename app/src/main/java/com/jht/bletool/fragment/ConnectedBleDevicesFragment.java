package com.jht.bletool.fragment;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.jht.bletool.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ConnectedBleDevicesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ConnectedBleDevicesFragment extends Fragment {
    private static final String TAG = "ConnectedBleDevicesFrag";
    private Context mContext;
    private Handler uiHandler;

    private ListView mLvConnectedDevices;
    private LinearLayout mIvEmptyView;

    public ConnectedBleDevicesFragment(Context context, Handler uiHandler) {
        this.mContext = context;
        this.uiHandler = uiHandler;
    }

    public static ConnectedBleDevicesFragment newInstance(Context context, Handler uiHandler) {
        ConnectedBleDevicesFragment fragment = new ConnectedBleDevicesFragment(context,uiHandler);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_connected_ble, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mLvConnectedDevices = view.findViewById(R.id.lv_connected_devices);
        mIvEmptyView = view.findViewById(R.id.iv_empty_view);
        //设置空列表的时候，显示为一张图片
        mLvConnectedDevices.setEmptyView(mIvEmptyView);
        mLvConnectedDevices.setAdapter(null);

    }
}