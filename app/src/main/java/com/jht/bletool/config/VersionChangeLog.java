package com.jht.bletool.config;

import com.jht.bletool.R;

public class VersionChangeLog {

    public static final String[] titles = new String[]{"Record command","How to send and delete commands?","Record the commands sent"};
    public static final String[] contents = new String[]{
            "The record button records commands.Head circle image button view all commands",
            "Play button send command, delete button delete command.",
            "The log will print the commands sent from the record."
    };
    public static final int[] imgs = new int[]{
        R.drawable.change_scan_record2,R.drawable.change_slide_downward2,R.drawable.change_log_data3};
}
