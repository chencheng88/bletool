package com.jht.bletool.config;

// app 的所有配置信息都通过此类获取

import android.util.Log;

import com.jht.bletool.util.BaseUtils;

import java.io.UnsupportedEncodingException;

public class AppConfig {
    private static final String TAG = "AppConfig";
    public static long scan_time ;
    public static final String record_data = "record_data";
    public static void init(long time){
        scan_time = time;
    }

    //获取蓝牙扫描时间
    public long getScanTime(){
        //4a - 46 - 49 - 43 - 43 - 59 - 43 - 4c - 45
//        byte[] value = new byte[]{0x4a,0x46,0x49,0x43,0x43,0x59,0x43,0x4c,0x45};
//        try {
//            Log.i(TAG, "getScanTime: " + new String(value, "UTF-8"));
//        } catch (UnsupportedEncodingException e) {
//            Log.i(TAG, "getScanTime: error");
//            e.printStackTrace();
//        }
        return scan_time;
    }

    public static String getHexString(byte[] data){
        StringBuilder sb = new StringBuilder();
        for (int i=0;i<data.length;i++){
            String str = Integer.toHexString(BaseUtils.byte1ToInt(data[i]));
            if (str.length() == 1){
                str = "0"+str;
            }
            sb.append(str +" - ");
        }
        return sb.toString();
    }

}
