package com.jht.bletool.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;

import java.util.HashMap;

public class BleService extends Service {
    private static final String TAG = "BleService";

    private HashMap data = new HashMap();

    public BleService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        startForegroundService();
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private void startForegroundService()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            String id = "id";
            CharSequence name = "BLEService";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(id, name, importance);
            mNotificationManager.createNotificationChannel(channel);

            Notification notification = new Notification.Builder(this, "id")
                    .setContentTitle("BLEService")
                    .setContentText("BLEService")
                    .setSmallIcon(android.R.drawable.sym_def_app_icon)
                    .build();
            startForeground(1, notification);
        } else {
            Notification notification = new Notification.Builder(this)
                    .setSmallIcon(android.R.drawable.sym_def_app_icon)
                    .build();
            startForeground(1, notification);
        }
    }
}
