package com.jht.bletool;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        System.out.println(toHex(1234,16));
        System.out.println(toHex(0,16));
        System.out.println(toHex(1234,2));
        System.out.println(toHex(0,2));
    }

    public static String toHex(int data,int radix){
        int temp = data;
        StringBuilder sb = new StringBuilder();
        switch (radix){
            case 2:
                System.out.println("================2================");
                while(temp/2 >= 0){
                    int buffer = temp%2;
                    String c = "";
                    switch(buffer){
                        case 0:
                            c = "0";
                            break;
                        case 1:
                            c = "1";
                            break;
                    }
                    sb.insert(0,c);
                    if (temp/2 == 0){
                        break;
                    }
                    temp = temp/2;
                }
                break;
            case 16:
                System.out.println("================16================");
                while(temp/16 >= 0){
                    int buffer = temp%16;
                    String c = "";
                    switch(buffer){
                        case 0:
                            c = "0";
                            break;
                        case 1:
                            c = "1";
                            break;
                        case 2:
                            c = "2";
                            break;
                        case 3:
                            c = "3";
                            break;
                        case 4:
                            c = "4";
                            break;
                        case 5:
                            c = "5";
                            break;
                        case 6:
                            c = "6";
                            break;
                        case 7:
                            c = "7";
                            break;
                        case 8:
                            c = "8";
                            break;
                        case 9:
                            c = "9";
                            break;
                        case 10:
                            c = "a";
                            break;
                        case 11:
                            c = "b";
                            break;
                        case 12:
                            c = "c";
                            break;
                        case 13:
                            c = "d";
                            break;
                        case 14:
                            c = "e";
                            break;
                        case 15:
                            c = "f";
                            break;

                    }
                    sb.insert(0,c);
                    if (temp/16 == 0){
                        break;
                    }
                    temp = temp/16;
                }
                break;
            default:
                return "00";
        }
        return sb.toString();
    }
}