package top.codestudy.annotation_uuid;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 用于表示蓝牙特征的注解，达到生成  特征和特征解析类 一一对应的映射类
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.CLASS)
public @interface MyUUID {
    String uuid();
}